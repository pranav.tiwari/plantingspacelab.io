
const smallViewport = 1439



// function viewPort() {
//     const width = $(window).width()
//     if (width <= 375) {
//         calc = (parseFloat(width) / parseFloat(375))
//         scale = 0.5 * calc
//         scale = scale.toFixed(2)
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=' + scale + ', maximum-scale=' + scale + ', width=750')
//     } else if (width <= smallViewport) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=750')
//     } else if (width > smallViewport && width < 1536) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=1440')
//     } else if (width >= 1536 && width < 1680) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=1536')
//     } else if (width >= 1680 && width < 1840) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=1680')
//     } else if (width >= 1840 && width < 1920) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=1840')
//     } else if (width >= 1920 && width < 2160) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=1920')
//     } else if (width >= 2160 && width < 2560) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=2160')
//     } else if (width >= 2560) {
//         $('meta[name=viewport]').attr('content', 'user-scalable=yes, initial-scale=1, maximum-scale=1, width=2560')
//     }

// }


const width = $(window).width()

if (width >= smallViewport) {
    $(".main-container").addClass("fixed")
    window.onload = function () {
        lax.init()

        // Add a driver that we use to control our animations
        lax.addDriver('scrollY', function () {
            return window.scrollY
        })

        // Add animation bindings to elements
        lax.addElements('.logo-big', {
            scrollY: {
                translateY: [
                    [0, 500],
                    [0, -500]
                ],
                scale: [
                    [0, 100],
                    [1, .4]
                ]
            }
        })
    }
}


function menu() {
    var scrollTop = $(window).scrollTop()
    var top = 426

    var force = $(".main-container").hasClass("joinus") || $(".main-container").hasClass("team")
    if (scrollTop > 80 || force || width >= smallViewport) {
        $(".main-container").addClass("fixed")
        $(".logo-big").css('top', '')
    } else {
        result = top - scrollTop
        $(".logo-big").css('top', result + 'px')
        $(".main-container").removeClass("fixed")
    }
}

menu()

$(window).scroll(function () {
    menu()
})

function showPersonInfo(e) {
    e.stopPropagation()
    e.preventDefault()
    $(".person-info").hide()
    $(".person-main-name").show()
    var _this = $(this)
    $(".person").addClass("zindex0")
    $(".person").removeClass("zindex")
    $(this).parent().parent().removeClass("zindex0")
    $(this).parent().parent().addClass("zindex")
    $(this).parent().parent().find(".person-main-name").fadeOut(100, function () {
        _this.parent().parent().find(".person-info").fadeIn(500)
    })
}

function hidePersonInfo(e) {
    e.stopPropagation()
    e.preventDefault()
    var _this = $(this)
    $(this).find(".person-info").fadeOut(200, function () {
        _this.find(".person-main-name").fadeIn(200)
    })
}

$(function () {
    if (width >= smallViewport) {
        $(".menu").addClass('collapsed')
    }
    $(document).on("click", ".menu-button", function () {
        $(".menu").toggleClass("collapsed")
    })
    $(document).on("mouseenter", ".person img", showPersonInfo)
    $(document).on("mouseenter", ".person .img-crop div", showPersonInfo)
    $(document).on("mouseleave", ".person", hidePersonInfo)
    $(document).on("mouseleave", ".person", hidePersonInfo)
    $(document).on("click", '.contact,a[href="#contact"]', function () {

        $('html, body').animate({
            scrollTop: $(".footer-header").offset().top
        }, 2000)
    })

    //load feed
    if ($(".feeds").length > 0) {
        var isAny = false
        $.ajax({
            url: "https://mas.to/users/PlantingSpace.rss",
            dataType: 'xml'
        }).done(function (xmlDoc) {
            $xml = $(xmlDoc)
            $item = $xml.find("item")
            $.each($xml.find("item"), function (index, value) {
                $date = $(this).find('pubDate').text().split(' ').slice(1, 4).toString().replace(/,/g, ' ')
                $txt = $(this).find('description').text()
                if ($txt != "") {
                    isAny = true
                    if (index == 0) {
                        $feed = $(".feed:eq(0)")
                        $feed.find(".feed-content").html($txt)
                        $feed.find(".feed-date").html($date)
                    } else {
                        $feed = $(".feed:eq(0)").clone()
                        $feed.find(".feed-content").html($txt)
                        $feed.find(".feed-date").html($date)
                        $(".feeds").append($feed)
                    }
                }
            })
            if (!isAny) {
                $(".feed:eq(0)").remove()
            }
        })
    }
})


$(function () {
    if (width >= smallViewport) {
        $(".menu").addClass('collapsed')
    }
    //load feed
})

